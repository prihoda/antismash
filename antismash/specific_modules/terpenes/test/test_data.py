import unittest
from os import path
from antismash import utils
import antismash.specific_modules.terpenes as terpenes

class TestTerpeneData(unittest.TestCase):
    def test_no_unicode_in_annotations(self):
        """Test terpene_synthase_annotations.txt doesn't contain unicode codepoints"""
        annotations_path = path.join(path.dirname(terpenes.__file__), "terpene_synthase_annotations.txt")
        self.assertTrue(path.exists(annotations_path))
        counter, line = utils.detect_unicode_in_file(annotations_path)
        self.assertEqual(counter, -1, "Invalid unicode literal in %r line %s: %r" % (annotations_path, counter+1, line))

    def test_no_unicode_in_ann_vs_acc(self):
        """Test terpene_synthase_annotations_vs_accessions.txt doesn't contain unicode codepoints"""
        annotations_path = path.join(path.dirname(terpenes.__file__), "terpene_synthase_annotations_vs_accessions.txt")
        self.assertTrue(path.exists(annotations_path))
        counter, line = utils.detect_unicode_in_file(annotations_path)
        self.assertEqual(counter, -1,
                         "Invalid unicode literal in %r line %s: %r" % (annotations_path, counter + 1, line))
