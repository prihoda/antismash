#!/bin/env python
# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import sys, os
import shutil

from multiprocessing import Process
import logging
from ..bin import allpred_nodep_par

def sandpuma_mt(filename):
    dir_name = os.path.splitext(filename)[0].rsplit('/', 1)[-1]
    ## Make subdir
    if not os.path.isdir(dir_name):
        os.mkdir(dir_name)
    ## Copy subset to the subdir
    shutil.copy(filename, dir_name)
    ## Run SANDPUMA
    allpred_nodep_par.main(os.path.join(dir_name, filename))

def run_parallel_fasttree(files):
    jobs = []
    for filename in files:
        p = Process(target=sandpuma_mt, args=(filename,))
        jobs.append(p)
        p.start()

    errors = []
    for job in jobs:
        job.join()
        if job.exitcode:
            logging.error("run_parallel_fastree: child %d exited with code %d",
                          job.pid, job.exitcode)
            errors.append((job.pid, job.exitcode))
    if errors:
        raise RuntimeError("child process %d exited with code %d" % (
                               errors[0][0], errors[0][1]))

if __name__ == "__main__":
    run_parallel_fasttree(sys.argv[1:])
