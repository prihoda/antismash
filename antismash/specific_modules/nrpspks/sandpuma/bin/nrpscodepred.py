# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

import os
import tempfile

from antismash import utils

__FILE__ = __file__
REF_SEQUENCE = "P0C062_A1"
A34_POSITIONS_FILENAME = utils.get_full_path(__FILE__, "A34positions.txt")
APOSITION_FILENAME = utils.get_full_path(__FILE__, "Apositions.txt")
KNOWN_CODES = utils.get_full_path(__FILE__, "knowncodes.fasta")
ILLEGAL_CHARS = "!@#$%^&*(){}:\"<>?/.,';][`~1234567890*-+-=_\\|"
ADOMAINS_FILENAME = utils.get_full_path(__FILE__, "A_domains_muscle.fasta")
START_POSITION = 66

def read_positions(filename, start_position):
    data = open(filename, "r")
    text = data.read().strip()
    results = []
    for i in text.split("\t"):
        results.append(int(i) - start_position)
    data.close()
    return results

def build_position_list(positions, reference_seq):
    poslist = []
    i = 0
    position = 0
    while i < len(reference_seq):
        if reference_seq[i] != "-":
            if position in positions:
                poslist.append(i)
            position += 1
        i += 1
    return poslist

def clean_seq(sequence):
    for char in ILLEGAL_CHARS:
        sequence = sequence.replace(char, "X")
    return sequence

def extract(query_seq, poslist):
    seq = []
    for position in poslist:
        aa = query_seq[position]
        if aa == "-":
            if position - 1 not in poslist:
                aa = query_seq[position - 1]
            elif position + 1 not in poslist:
                aa = query_seq[position + 1]
        seq.append(aa)
    return "".join(seq)

def run_muscle(query):
    """ run muscle over the query and known A domains """
    # using mkstemp() here because NamedTemporaryFiles cause muscle
    # to think the file is infinite and use > 20Gb of RAM per invocation
    in_fd, in_filename = tempfile.mkstemp(prefix="antismash_musc")
    in_handle = os.fdopen(in_fd, "w")
    in_handle.write(">{}\n{}\n".format(query[0], clean_seq(query[1])))
    in_handle.close()
    out_fd, out_filename = tempfile.mkstemp(prefix="antismash_musc")
    os.close(out_fd)
    try:
        utils.execute(["muscle", "-profile", "-quiet",
                       "-in1", ADOMAINS_FILENAME,
                       "-in2", in_filename,
                       "-out", out_filename])
    except:
        raise
    else:
        results = utils.read_fasta(out_filename)
    finally:
        os.unlink(in_filename)
        os.unlink(out_filename)
    return results

def run_nrpscodepred(query_sequence, filename=None):
    """ Extract 10 / 34 AA NRPS signatures from A domains """
    query_seq_id = query_sequence[0].replace(" ", "_")
    query_sequence = (query_seq_id, query_sequence[1])
    #Run muscle and collect sequence positions from file
    muscle = run_muscle(query_sequence)

    positions = read_positions(APOSITION_FILENAME, START_POSITION)
    #Count residues in ref sequence and put positions in list
    poslist = build_position_list(positions, muscle[REF_SEQUENCE])

    #Extract positions from query sequence
    query_sig_seq = extract(muscle[query_seq_id], poslist)
    #Add fixed lysine 517
    query_sig_seq = query_sig_seq + "K"

    #repeat with 34 AA codes
    angpositions = read_positions(A34_POSITIONS_FILENAME, START_POSITION)
    poslist = build_position_list(angpositions, muscle[REF_SEQUENCE])
    querysig34code = extract(muscle[query_seq_id], poslist)

    if filename is not None:
        output(querysig34code, query_seq_id, filename)
    return (querysig34code, query_seq_id)

def output(querysig34code, query_seq_id, filename):
    if type(query_seq_id) != str or type(querysig34code) != str:
        raise TypeError("Query sequence id and sequence must be strings")

    outfile = open(filename, "w")
    outfile.write("{}\t{}\n".format(querysig34code, query_seq_id))
    outfile.close()

