from __future__ import print_function, division

from sklearn import tree
import csv
import sys

def _read_features(filename="etmp.features.tsv"):
    features = []
    with open(filename, 'rb') as tsvinf:
        tsvin = csv.reader(tsvinf, delimiter='\t')
        for row in tsvin:
            features.append(row)
    return features

def _read_labels(filename="etmp.labels.tsv"):
    labels = []
    with open(filename, 'rb') as tsvinf:
        tsvin = csv.reader(tsvinf, delimiter='\t')
        for row in tsvin:
            labels.append(row[0])
    return labels

def main(qmat, max_depth=None, msl=1, features=None, labels=None):
    if max_depth is not None and type(max_depth) != int:
        raise TypeError("max_depth is required to be an int or None")
    if type(msl) != int:
        raise TypeError("msl is required to be an int")

    if features is None:
        features = _read_features()
    if labels is None:
        labels = _read_labels()

    if len(features) != len(labels):
        raise ValueError("feature length (%d) and label length (%d) mismatch" % (len(features), len(labels)))

    ## Train the decision tree
    clf = tree.DecisionTreeClassifier(presort=True, min_samples_leaf=msl, max_depth=max_depth)
    clf = clf.fit(features, labels)

    ## Make prediction
    return clf.predict([qmat])[0]

if __name__ == "__main__":
    print(main(sys.argv[1:-2], max_depth=int(sys.argv[-2]), msl=int(sys.argv[-1])))
