# set fileencoding: utf-8
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from __future__ import print_function, division

from .bin import rescore, parbreakup

def main(sequence_info, cores):
    if type(sequence_info) != dict:
        raise TypeError("sequence info should be a dict of name->sequence")
    parbreakup.main(sequence_info, cores)
    sand_out = open("sandpuma.tsv", "w")
    results = rescore.main("pid.res.tsv", "ind.res.tsv", "ens.res.tsv")
    for line in results:
        sand_out.write(line + '\n')
    sand_out.close()

