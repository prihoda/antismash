# set fileencoding: utf-8
import shutil
import tempfile
import unittest
from argparse import Namespace
from minimock import mock, restore
from os import path

from antismash import utils
from antismash.config import Namespace
from antismash.specific_modules.nrpspks import parsers

class TestNRPSParser(unittest.TestCase):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.options = Namespace()
        self.pksnrpsvars = Namespace()

        self.options.raw_predictions_outputfolder = self.tmpdir
        self.options.record_idx = 1

        self.pksnrpsvars.nrpsnames = ['demO_A1', 'demO_A2']

        with open(path.join(self.tmpdir, 'ctg1_ind.res.tsv'), 'w') as fh:
            fh.write("demO_A1\tprediCAT_MP\tQ939Z1_A1\n")
            fh.write("demO_A1\tprediCAT_SNN\tno_force_needed\t1.234567\n")
            fh.write("demO_A1\tASM\tleu\n")
            fh.write("demO_A1\tSVM\tleu\n")
            fh.write("demO_A1\tpHMM\tleu\n")
            fh.write("demO_A2\tprediCAT_MP\tQ939Z1_A2\n")
            fh.write("demO_A2\tprediCAT_SNN\tno_force_needed\t2.345678\n")
            fh.write("demO_A2\tASM\tbht|tyr\n")
            fh.write("demO_A2\tSVM\tbht\n")
            fh.write("demO_A2\tpHMM\tbht\n")

        with open(path.join(self.tmpdir, 'ctg1_pid.res.tsv'), 'w') as fh:
            fh.write("demO_A1\t42.00\n")
            fh.write("demO_A2\t23.00\n")


        with open(path.join(self.tmpdir, 'ctg1_sandpuma.tsv'), 'w') as fh:
            fh.write("demO_A1\tSANDPUMA\tno_call\n")
            fh.write("demO_A2\tSANDPUMA\tbht\n")

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_parser(self):
        expected_allmethods = {
            'demO_A1': {
                'prediCAT_MP': 'Q939Z1_A1',
                'prediCAT_SNN': 'no_force_needed',
                'ASM': 'leu',
                'SVM': 'leu',
                'pHMM': 'leu',
            },
            'demO_A2': {
                'prediCAT_MP': 'Q939Z1_A2',
                'prediCAT_SNN': 'no_force_needed',
                'ASM': 'bht|tyr',
                'SVM': 'bht',
                'pHMM': 'bht',
            }
        }

        expected_snn = {
            'demO_A1': '1.234567',
            'demO_A2': '2.345678'
        }

        expected_pid = {
            'demO_A1': '42.00',
            'demO_A2': '23.00'
        }

        expected_res = {
            'demO_A1': 'no_call',
            'demO_A2': 'bht'
        }

        parsers.parse_nrps_preds(self.options, self.pksnrpsvars)
# silence false-positives for Namespace attributes
# pylint: disable=no-member
        assert 'sandpuma_allmethods' in self.pksnrpsvars
        self.assertEqual(expected_allmethods, self.pksnrpsvars.sandpuma_allmethods)
        assert 'sandpuma_snn' in self.pksnrpsvars
        self.assertEqual(expected_snn, self.pksnrpsvars.sandpuma_snn)
        assert 'sandpuma_pid' in self.pksnrpsvars
        self.assertEqual(expected_pid, self.pksnrpsvars.sandpuma_pid)
        assert 'sandpuma_res' in self.pksnrpsvars
        self.assertEqual(expected_res, self.pksnrpsvars.sandpuma_res)
# pylint: enable=no-member


class TestNRPSParserMonomerModification(unittest.TestCase):
    def setUp(self):
        class DummyFeature(object):
            def __init__(self, val):
                self.qualifiers = {'product': [val]}
        self.dummy = [[DummyFeature('not_atpks')], [DummyFeature('transatpks')]]
        self.predictions = ['redmxmal', 'ccmal', 'ohemal', 'ohmxmal', 'ohmmal',
                            'ccmmal', 'emal', 'redmmal', 'mmal', 'ccmxmal',
                            'mxmal', 'redemal', 'ohmal', 'mal', 'ccemal']
        mock('utils.get_cluster_features', returns_iter=self.dummy*len(self.predictions))

    def tearDown(self):
        restore()

    def gen_domain_names(self):
        res = {}
        possible_values = ['nomatch', 'PKS_AT', 'PKS_KR', 'PKS_KS', 'PKS_DH', 'PKS_ER']
        for n, v in enumerate(possible_values):
            key = ord('a') + n
            res[chr(key)] = [v]
            res[chr(key + len(possible_values))] = [v]*3
        res['all'] = possible_values*3
        return res

    def gen_predictions(self, domains, pred):
        res = {}
        for domain, values in domains.items():
            for pks_type in ["AT", "KS"]:
                positions = parsers.find_duplicate_position(values, "PKS_" + pks_type)
                for pos in positions:
                    res["{}_{}{}".format(domain, pks_type, pos + 1)] = pred
        res['all_KS1'] = pred
        res['all_KS2'] = pred
        res['all_KS3'] = pred
        res['all_AT1'] = pred
        res['all_AT2'] = pred
        res['all_AT3'] = pred
        return res

    def detect_change(self, preds, original, expected):
        changed = []
        for domain, new in preds.items():
            if new != original:
                changed.append((domain, new))
        assert changed == expected

    def test_insert_modified_monomers(self):
        dummy_vars = Namespace()
        dummy_vars.domainnamesdict = self.gen_domain_names()

        expected_changes = [([], []), # redmxmal
            ([('all_AT3', 'redmal'), ('all_AT1', 'redmal'), ('all_AT2', 'redmal')], [('all_KS2', 'redmal'), ('all_KS3', 'redmal'), ('all_KS1', 'redmal')]), # ccmal
            ([('all_AT3', 'redemal'), ('all_AT1', 'redemal'), ('all_AT2', 'redemal')], [('all_KS2', 'redemal'), ('all_KS3', 'redemal'), ('all_KS1', 'redemal')]), # ohemal
            ([('all_AT3', 'redmxmal'), ('all_AT1', 'redmxmal'), ('all_AT2', 'redmxmal')], [('all_KS2', 'redmxmal'), ('all_KS3', 'redmxmal'), ('all_KS1', 'redmxmal')]), # ohmxmal
            ([('all_AT3', 'redmmal'), ('all_AT1', 'redmmal'), ('all_AT2', 'redmmal')], [('all_KS2', 'redmmal'), ('all_KS3', 'redmmal'), ('all_KS1', 'redmmal')]), # ohmmal
            ([('all_AT3', 'redmmal'), ('all_AT1', 'redmmal'), ('all_AT2', 'redmmal')], [('all_KS2', 'redmmal'), ('all_KS3', 'redmmal'), ('all_KS1', 'redmmal')]), # ccmmal
            ([('all_AT3', 'redemal'), ('all_AT1', 'redemal'), ('all_AT2', 'redemal')], [('all_KS2', 'redemal'), ('all_KS1', 'redemal')]), # emal
            ([], []), # redmmal
            ([('all_AT3', 'redmmal'), ('all_AT1', 'redmmal'), ('all_AT2', 'redmmal')], [('all_KS2', 'redmmal'), ('all_KS1', 'redmmal')]), # mmal
            ([('all_AT3', 'redmxmal'), ('all_AT1', 'redmxmal'), ('all_AT2', 'redmxmal')], [('all_KS2', 'redmxmal'), ('all_KS3', 'redmxmal'), ('all_KS1', 'redmxmal')]), # ccmxmal
            ([('all_AT3', 'redmxmal'), ('all_AT1', 'redmxmal'), ('all_AT2', 'redmxmal')], [('all_KS2', 'redmxmal'), ('all_KS1', 'redmxmal')]), # mxmal
            ([], []), # redemal
            ([('all_AT3', 'redmal'), ('all_AT1', 'redmal'), ('all_AT2', 'redmal')], [('all_KS2', 'redmal'), ('all_KS3', 'redmal'), ('all_KS1', 'redmal')]), # ohmal
            ([('all_AT3', 'redmal'), ('all_AT1', 'redmal'), ('all_AT2', 'redmal')], [('all_KS2', 'redmal'), ('all_KS1', 'redmal')]), # mal
            ([('all_AT3', 'redemal'), ('all_AT1', 'redemal'), ('all_AT2', 'redemal')], [('all_KS2', 'redemal'), ('all_KS3', 'redemal'), ('all_KS1', 'redemal')])] # ccemal

        for n, pred in enumerate(self.predictions):
            # twice for each, for not_transatpks and transatpks
            dummy_vars.consensuspreds = self.gen_predictions(dummy_vars.domainnamesdict, pred)
            parsers.insert_modified_monomers(dummy_vars, None)
            self.detect_change(dummy_vars.consensuspreds, pred, expected_changes[n][0])
            dummy_vars.consensuspreds = self.gen_predictions(dummy_vars.domainnamesdict, pred)
            parsers.insert_modified_monomers(dummy_vars, None)
            self.detect_change(dummy_vars.consensuspreds, pred, expected_changes[n][1])
