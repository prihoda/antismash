#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

PREFIX=${1}/antismash/generic_modules/hmm_detection

pushd ${PREFIX}

grep ".hmm" hmmdetails.txt | awk -F $'\t' '{print $4}' | xargs cat > bgc_seeds.hmm

popd
